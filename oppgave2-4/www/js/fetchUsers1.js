function fetchUsers() {
  fetch ('api/fetchUsers.php')
  .then(res=>res.json())
  .then(users=>{
    const userUL = document.querySelector('.users ul');
    userUL.innerHTML = '';
    users.forEach(user=>{
      const userLI = document.createElement ('LI');
      userLI.setAttribute ('data-uid', user.uid);
      // The line below has changed from "oppgave3.html"
      userLI.innerHTML = `<show-user uname="${user.uname}" uid="${user.uid}" firstName="${user.firstName}" lastName="${user.lastName}" hasAvatar="${user.hasAvatar}"></show-user>`;
      userUL.appendChild (userLI);
    })
    userUL.addEventListener('click', e=>{
      if (e.path[3].tagName=='LI') {          // idx changed from 1 to 3 from "oppgave3.html"
        editUser (e.path[3].dataset['uid']);  // idx changed from 1 to 3 from "oppgave3.html"
      } else if (e.target.tagName=='LI') {
        editUser (e.target.dataset['uid']);
      }
    })
  })
}

fetchUsers();
