<?php
/**
 * Used to receive a file containing a new avatar image for a given user.
 * The user id should be placed in the header field *X-uid* and the user name should
 * be placed in the header field *X-uname*. Use XMLHttpRequest and send the file
 * as a file.
 *
 * This script will accept the file and handle it as an image and rescale it to
 * a maximum size of 150x150 pixels. This image will then be stored as the Avatar
 * image for the user with the given user id and user name (must match).
 */
header("Content-type: application/json");           // Send back json data
require_once ('../classes/DB.php');

$uid = $_SERVER['HTTP_X_UID'];       // Get extra parameters
$uname = $_SERVER['HTTP_X_UNAME'];

$handle = fopen('php://input', 'r');                // Read the file from stdin
$content = '';

while (!feof($handle)) {                            // Read in blocks of 8 KB (no file size limit)
    $content .= fread($handle, 8192);
}
fclose($handle);

$height = 150;
$width = 150;
$avatar = scale (imagecreatefromstring($content), $width, $height);
unset ($content);     // Free up memory from old/unscaled image

$db = DB::getDBConnection();
$sql = 'UPDATE user set avatar=? WHERE uname=? and uid=?';
$stmt = $db->prepare($sql);
$stmt->execute(array($avatar, $uname, $uid));
if ($stmt->rowCount()==1) {
  echo json_encode(array('status'=>'success'));
} else {
  echo json_encode(array('status'=>'fail'));
}

function scale ($img, $new_width, $new_height) {
  $old_x = imageSX($img);
  $old_y = imageSY($img);

  if($old_x > $old_y) {                     // Image is landscape mode
    $thumb_w = $new_width;
    $thumb_h = $old_y*($new_height/$old_x);
  } else if($old_x < $old_y) {              // Image is portrait mode
    $thumb_w = $old_x*($new_width/$old_y);
    $thumb_h = $new_height;
  } if($old_x == $old_y) {                  // Image is square
    $thumb_w = $new_width;
    $thumb_h = $new_height;
  }

  if ($thumb_w>$old_x) {                    // Don't scale images up
    $thumb_w = $old_x;
    $thumb_h = $old_y;
  }

  $dst_img = ImageCreateTrueColor($thumb_w,$thumb_h);
  imagecopyresampled($dst_img,$img,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y);

  ob_start();                         // flush/start buffer
  imagepng($dst_img,NULL,9);          // Write image to buffer
  $scaledImage = ob_get_contents();   // Get contents of buffer
  ob_end_clean();                     // Clear buffer
  return $scaledImage;
}
